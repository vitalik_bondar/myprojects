// document.addEventListener('DOMContentLoaded', showTable);
var pageNumber = 1;
var peopleListCounter=1;
var table_person_list = document.getElementById('table_person_list');
var titleOfTable = document.getElementById('title-of-table');
var movieList = document.getElementById('movie-list');
var homeWorld = document.getElementById('home-world');
var speciesPerson = document.getElementById('species-person');
var showLi = document.getElementById('show-person-li');

function showTable() {
    showLoader_list_people();
    var req = new XMLHttpRequest();
    var URLhost = 'https://swapi.co/api/people/?page=' + pageNumber;
    req.open('GET', URLhost, true);
    req.addEventListener('load', function () {
        if (req.status >= 200 && req.status < 400) {
            var response = JSON.parse(req.responseText);
            console.log(response);
            titleOfTable.innerText="LIST OF PEOPLE";
            createTitleTablePersonList();
            for (var i = 0; i < response.results.length; i++) {
                var tr=document.createElement('tr');
                var tdId=document.createElement('td');
                var tdName=document.createElement('td');
                var tdHeight=document.createElement('td');
                var tdMass=document.createElement('td');
                var tdBirthYear=document.createElement('td');
                var tdGender=document.createElement('td');
                var tdUrl=document.createElement('td');
                var showUrl=document.createTextNode(response.results[i].url);
                var showId=document.createTextNode((pageNumber-1)*10+(i+1));
                var showName=document.createTextNode(response.results[i].name);
                var showHeight=document.createTextNode(response.results[i].height);
                var showMass=document.createTextNode(response.results[i].mass);
                var showBirthYear=document.createTextNode(response.results[i].birth_year);
                var showGender=document.createTextNode(response.results[i].gender);
                tdUrl.appendChild(showUrl);
                tdId.appendChild(showId);
                tdName.appendChild(showName);
                tdHeight.appendChild(showHeight);
                tdMass.appendChild(showMass);
                tdBirthYear.appendChild(showBirthYear);
                tdGender.appendChild(showGender);
                tdUrl.style.display="none";
                tr.appendChild(tdUrl);
                tr.appendChild(tdId);
                tr.appendChild(tdName);
                tr.appendChild(tdHeight);
                tr.appendChild(tdMass);
                tr.appendChild(tdBirthYear);
                tr.appendChild(tdGender);
                table_person_list.appendChild(tr);
                tr.addEventListener("click",getUrlPeople);
                tr.style.cursor="pointer";

            }
            var nextBtn=document.createElement('button');
            var prevBtn=document.createElement('button');
            var btnPrevText=document.createTextNode("<<Previous");
            var btnNextText=document.createTextNode("Next>>");
            nextBtn.appendChild(btnNextText);
            prevBtn.appendChild(btnPrevText);
            prevBtn.addEventListener("click",prev);
            prevBtn.setAttribute("id","prev");
            nextBtn.setAttribute("id","next");
            nextBtn.addEventListener("click",next);
            table_person_list.appendChild(prevBtn);
            table_person_list.appendChild(nextBtn);
            hideLoader_list_people();
            if (pageNumber <= 1) {
                document.getElementById('prev').setAttribute("disabled", "true");
            } else {
                document.getElementById('prev').removeAttribute("disabled");
            }
            if (pageNumber >= 9) {
                document.getElementById('next').setAttribute("disabled", "");
            } else {
                document.getElementById('next').removeAttribute("disabled");
            }
        } else {
            console.log('Error in network request: ' + req.statusText);
        }
    });
    req.send();

}

function next() {
    if (pageNumber < 9) {
        table_person_list.innerText="";
        pageNumber++;
        showTable();
    }
}

function prev() {
    if (pageNumber > 1) {
        table_person_list.innerText="";
        pageNumber--;
        showTable();
    }
}
function createTitleTablePersonList() {
    var tr=document.createElement('tr');
    var thId=document.createElement('th');
    var thName=document.createElement('th');
    var thHeight=document.createElement('th');
    var thMass=document.createElement('th');
    var thBirthYear=document.createElement('th');
    var thGender=document.createElement('th');
    var showId=document.createTextNode('#');
    var showName=document.createTextNode('Name');
    var showHeight=document.createTextNode('Height');
    var showMass=document.createTextNode('Mass');
    var showBirthYear=document.createTextNode('Birth Year');
    var showGender=document.createTextNode('Gender');
    thId.appendChild(showId);
    thName.appendChild(showName);
    thHeight.appendChild(showHeight);
    thMass.appendChild(showMass);
    thBirthYear.appendChild(showBirthYear);
    thGender.appendChild(showGender);
    tr.appendChild(thId);
    tr.appendChild(thName);
    tr.appendChild(thHeight);
    tr.appendChild(thMass);
    tr.appendChild(thBirthYear);
    tr.appendChild(thGender);
    tr.style.fontSize="36px";
    table_person_list.appendChild(tr);
    table_person_list.setAttribute("class","table");
}

function onOffPeopleList() {
    peopleListCounter++;
    if(peopleListCounter%2==0){
        showTable();
        showLi.innerHTML="CLOSE INFO ABOUT PERSON";
    } else{
        table_person_list.innerText="";
        titleOfTable.innerText="";
        showLi.innerHTML="SHOW INFO ABOUT PERSON";
    }
}
function getUrlPeople() {
    var urlPerson= this.childNodes[0].innerHTML;
    showInfoPerson();
    createInfoPerson(urlPerson);
    // alert(surname);
}
// end of people list table
// start person information table
var dark = document.getElementById("dark");
var infoPerson = document.getElementById("info-person");

function closeInfoPerson() {
    dark.style.display="none";
    infoPerson.style.display="none";
}
function showInfoPerson() {
    dark.style.display="block";
    infoPerson.style.display="block";
}
function createInfoPerson(urlPerson) {
    var req = new XMLHttpRequest();
    var URL = urlPerson;
    req.open('GET', URL, true);
    req.addEventListener('load', function () {
        if (req.status >= 200 && req.status < 400) {
            var response = JSON.parse(req.responseText);
            console.log(response);
            // infoPerson.innerText="";
            var spans = document.querySelectorAll('#info-person span');
            for (var i=0;i<spans.length;i++){
                spans[i].innerText='';
            }
            var personName=document.createTextNode(response.name);
            var personHeight=document.createTextNode(response.height);
            var personMass=document.createTextNode(response.mass);
            var personHairColor=document.createTextNode(response.hair_color);
            var personSkinColor=document.createTextNode(response.skin_color);
            var personEyeColor=document.createTextNode(response.eye_color);
            var personBirthYear=document.createTextNode(response.birth_year);
            var personGender=document.createTextNode(response.gender);

            spans[0].appendChild(personName);
            spans[1].appendChild(personHeight);
            spans[2].appendChild(personMass);
            spans[3].appendChild(personHairColor);
            spans[4].appendChild(personSkinColor);
            spans[5].appendChild(personEyeColor);
            spans[6].appendChild(personBirthYear);
            spans[7].appendChild(personGender);

            //begin home world
            var homeReq = new XMLHttpRequest();
            var planetURL = response.homeworld;
            homeReq.open('GET', planetURL, true);
            homeReq.addEventListener('load', function () {
                if (homeReq.status >= 200 && homeReq.status < 400) {
                    var planetResponse = JSON.parse(homeReq.responseText);
                    var personHome =document.createElement("span");
                    personHome.textContent= planetResponse.name;
                    homeWorld.appendChild(personHome);
                } else {
                    console.log('Error in network request: ' + req.statusText);
                }});
            homeReq.send();
            //end of home world

            //begin species
            if (response.species.length >0){
            var speciesReq = new XMLHttpRequest();
            var planetURL = response.species;
            speciesReq.open('GET', planetURL, true);
            speciesReq.addEventListener('load', function () {
                if (speciesReq.status >= 200 && speciesReq.status < 400) {
                    var planetResponse = JSON.parse(speciesReq.responseText);
                    var personSpecies =document.createElement("span");
                    personSpecies.textContent= planetResponse.name;
                    speciesPerson.appendChild(personSpecies);
                } else {
                    console.log('Error in network request: ' + req.statusText);
                }});
            speciesReq.send();
            } else {
             var personSpeciesNone = document.createElement('span');
             personSpeciesNone.textContent="none";
             speciesPerson.appendChild(personSpeciesNone);
            }
            //end species

            //begin films list of person
            if (response.films.length > 0) {
                for (var e = 0; e < response.films.length; e++) {
                    (function () {
                        var newURLhost = response.films[e];
                        var newReq = new XMLHttpRequest();
                        newReq.open('GET', newURLhost, true);
                        newReq.addEventListener('load', function(){
                            if(newReq.status >= 200 && newReq.status < 400){
                                var newResponse = JSON.parse(newReq.responseText);
                                var movie = document.createElement('span');
                                movie.textContent = newResponse.title;
                                movieList.appendChild(movie);
                                hideLoader();
                            } else {
                                console.log("Error in network request: " + newReq.statusText);
                            }});
                        newReq.send();

                        })();

                }
            }


} else {
            console.log('Error in network request: ' + req.statusText);
        }
    });

    req.send();
    showLoader();

}
//loaders
function showLoader () {
    document.getElementsByClassName("loader")[0].style.display = "block";
    var wait = document.getElementById("info-person");
    wait.style.display = "none";
}
function hideLoader() {
    document.getElementsByClassName("loader")[0].style.display = "none";
    var wait = document.getElementById("info-person");
    wait.style.display = "block";
}

function showLoader_list_people() {
    var loader= document.getElementsByClassName("loader")[0];
    loader.style.display = "block";
    loader.style.background='none';
    titleOfTable.style.display='none';
}
function hideLoader_list_people() {
    document.getElementsByClassName("loader")[0].style.display = "none";
    titleOfTable.style.display='block';
}

